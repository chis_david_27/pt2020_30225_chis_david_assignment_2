import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

public class CazGeneral implements Runnable {

	private Scheduler scheduler;

	public static ArrayList<Task> generated_Tasks;
	public static Input in;

	public CazGeneral(String path, String path2) throws FileNotFoundException {
		try {
			in = new Input(path, path2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		scheduler = new Scheduler(in.getNo_queues(), in.getNo_client());

		generate_N_Random_Tasks();
	}

	private void afisare_generated_Tasks() throws IOException {

		
		
		in.getWriter().write("Lista de clienti care asteapta sa intre in cozi este\n\n");
		for (Task i : generated_Tasks) {
		//	System.out.print(i.toString());
			in.getWriter().write(" "+i.toString());
		}
		
	}

	private static void generate_N_Random_Tasks() throws FileNotFoundException {

		Random r = new Random();
		generated_Tasks = new ArrayList<Task>();
		for (int i = 0; i < in.getNo_client(); i++) {
			generated_Tasks.add(new Task(i + 1, r.nextInt(in.getArrival_time_max()) + in.getArrival_time_min(),
					r.nextInt(in.getService_time_max()) + in.getService_time_min()));
		}
	}

	@Override
	public void run() {
		//
		int time = 0;
		int suma = 0;
		for (Task i : generated_Tasks) {
			suma += i.getProcessingTime();
		}
		while (time < in.getInterval_sim()) {
			//System.out.printf("\nTime =%d \n", time);
			try {
				in.getWriter().write("\n\n Time =" + time + "\n\n");
			} catch (IOException e1) {
				
				e1.printStackTrace();
			}
			Iterator<Task> iterat = generated_Tasks.iterator();
			// for (Task i: generated_Tasks)
			while (iterat.hasNext()) {
				Task new_task = iterat.next();
		
				if (new_task.getArrivalTime() == time) {
					scheduler.dispatchTask(new_task);
					iterat.remove();}
			}
			time++;
			try {
				afisare_generated_Tasks();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			scheduler.afis_servers(in.getWriter());
			int nr = 0;
			if (generated_Tasks.isEmpty()) {
				for (Server i : scheduler.getServer()) {
					if (i.getTasks().isEmpty())

					{
						nr++;
					}
				}
				if (nr == in.getNo_queues()) {
					double average_time = (double) suma / in.getNo_client();
					//System.out.println(average_time);
					try {
						in.getWriter().write("\nAverage Time -> "+average_time+"\n");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}//("\nAverage Time -> ");
					
					break;
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		try {
			in.getWriter().close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws IOException // throws FileNotFoundException
	{

		Thread t;
		try {
		CazGeneral caz = new CazGeneral(args[0],args[1]);
		//	CazGeneral caz = new CazGeneral("C:\\Users\\asus\\Desktop\\Java\\Tema2 Cozi\\src\\input.txt","C:\\Users\\asus\\Desktop\\Java\\Tema2 Cozi\\src\\output2.txt");
			
			t = new Thread(caz);
			t.start();
			

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
