import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{

	private BlockingQueue<Task> tasks;//= new ArrayList<Task>();
	private AtomicInteger waitingPeriod;// = new AtomicInteger(0);	
	
	public Server( ) {
		super();
		this.tasks= new LinkedBlockingQueue<Task>();
		waitingPeriod=new AtomicInteger(0);
	
	}
	
	

	public BlockingQueue<Task> getTasks() {
		return tasks;
	}

	public void setTasks(BlockingQueue<Task> tasks) {
		this.tasks = tasks;
	}

	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}

	public void setWaitingPeriod(AtomicInteger waitingPeriod) {
		this.waitingPeriod = waitingPeriod;
	}

	public void addTask(Task t) {
		tasks.add(t);
		waitingPeriod.addAndGet(t.getProcessingTime());
	}
	@Override
	public void run() {

	
		while(!tasks.isEmpty())
		{
			
		    try {
		   
				Thread.sleep(1000);
				{
					setWaitingPeriod(new AtomicInteger(getWaitingPeriod().intValue()-1));
				
				//	if(!tasks.isEmpty())
					{
						tasks.peek().setProcessingTime(tasks.peek().getProcessingTime()-1);
					}
				    if(tasks.peek().getProcessingTime()==0)
				    {
				    	//System.out.println("s-a sters "+tasks.peek().toString());
				    	tasks.remove();
				    	
				    }
				}
			
				
			} catch (InterruptedException e) 
		    {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		}
		
	
	}

	public String toString()
	{
		String s="";
		if(tasks.isEmpty())
		{
			s+="Closed";
		}
		else
		{
		
		for(Task i:tasks)
		{	
			s+=i.toString();
		}
		s+="NULL";
		
	}
	


		return s;
	

	}}

