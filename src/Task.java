
public class Task {
public int id;
private int arrivalTime;
private int processingTime;


public Task(int id,int arrivalTime, int processingTime) {
	super();
	this.id=id;
	this.arrivalTime = arrivalTime;
	this.processingTime = processingTime;

}
public int getArrivalTime() {
	return arrivalTime;
}
public void setArrivalTime(int arrivalTime) {
	this.arrivalTime = arrivalTime;
}

public int getProcessingTime() {
	return processingTime;
}
public void setProcessingTime(int processingTime) {
	this.processingTime = processingTime;
}


   public String toString()
   {
	   return "("+id+","+arrivalTime+","+processingTime+")->";
   }


}
