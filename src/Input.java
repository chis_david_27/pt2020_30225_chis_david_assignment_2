import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Input {

	  private int no_client,no_queues,interval_sim,arrival_time_min,arrival_time_max,service_time_min,service_time_max;
	 private FileWriter writer;
	 
	
	public int getNo_client() {
		return no_client;
	}

	public FileWriter getWriter() {
		return writer;
	}


	public void setWriter(FileWriter writer) {
		this.writer = writer;
	}

	public void setNo_client(int no_client) {
		this.no_client = no_client;
	}


	public int getNo_queues() {
		return no_queues;
	}


	public void setNo_queues(int no_queues) {
		this.no_queues = no_queues;
	}


	public int getInterval_sim() {
		return interval_sim;
	}


	public void setInterval_sim(int interval_sim) {
		this.interval_sim = interval_sim;
	}


	public int getArrival_time_min() {
		return arrival_time_min;
	}


	public void setArrival_time_min(int arrival_time_min) {
		this.arrival_time_min = arrival_time_min;
	}


	public int getArrival_time_max() {
		return arrival_time_max;
	}


	public void setArrival_time_max(int arrival_time_max) {
		this.arrival_time_max = arrival_time_max;
	}


	public int getService_time_min() {
		return service_time_min;
	}


	public void setService_time_min(int service_time_min) {
		this.service_time_min = service_time_min;
	}


	public int getService_time_max() {
		return service_time_max;
	}


	public void setService_time_max(int service_time_max) {
		this.service_time_max = service_time_max;
	}


	public Input(String path,String path1) throws IOException 
	{
	//	path="C:\\Users\\asus\\Desktop\\Java\\Tema2 Cozi\\src\\input.txt"
		   File file = new File(path);
           writer = new FileWriter(path1);
    Scanner scan = new Scanner(file);
    int []v= new int[7];
    int k=0;
    while(scan.hasNext())
    {
    	
    	String s=scan.next();
    	if(s.contains(" "))
    	{
    		s=s.replace(" ", "");
    	}
    	if(s.contains(",")==true)
    	{
    		
    		String []arr=s.split(",");
    		v[k++]=Integer.parseInt(arr[0]);
    		v[k++]=Integer.parseInt(arr[1]);
    	}
    	else
    	{
    		v[k++]=Integer.parseInt(s);
    	}
    }
    
	
	this.no_client=v[0];
	this.no_queues=v[1];
	this.interval_sim=v[2];
	this.arrival_time_min=v[3];
	this.arrival_time_max=v[4];
	this.service_time_min=v[5];
	this.service_time_max=v[6];
	
	
	}


	
 


}
