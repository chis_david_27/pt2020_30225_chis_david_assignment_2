import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class Scheduler {

	 private static ArrayList<Server> servers;
	 private int maxNoServers;
	 private int maxTasksPerServer;
	
	
	 
	public int getMaxNoServers() {
		return maxNoServers;
	}

	public void setMaxNoServers(int maxNoServers) {
		this.maxNoServers = maxNoServers;
	}

	public int getMaxTasksPerServer() {
		return maxTasksPerServer;
	}

	public void setMaxTasksPerServer(int maxTasksPerServer) {
		this.maxTasksPerServer = maxTasksPerServer;
	}

	public void afis_servers(FileWriter f)
	{
		
		for(Server i : servers)
		{
			
			try {
				f.write("\nQueue  "+(servers.indexOf(i)+1)+"  "+i.toString());
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		//	System.out.print("\nQueue  "+(servers.indexOf(i)+1)+"  "+i.toString());
		   
		}
		
	}
     public static Server minim_server()
     {
    	 int min = servers.get(0).getWaitingPeriod().intValue();
    	
    	 Server s= servers.get(0);
    	 for(Server i : servers)
    	 {
    		 if(i.getWaitingPeriod().intValue()<min)
    		 {
    			 min=i.getWaitingPeriod().intValue();
    			 s=i;
    			 
    		 }
    	 }
		return s;
    	 
     }
 
	public Scheduler( int maxNoServers, int maxTasksPerServer) {
		super();
		this.servers=new ArrayList<Server>();
		for(int i=0;i<maxNoServers;i++)
		{
			servers.add(new Server());
		}
		this.maxNoServers = maxNoServers;
		this.maxTasksPerServer = maxTasksPerServer;

	}
	
	 public static void dispatchTask(Task t)
	 {
		Server minim = minim_server();
	
		int ok=0;
		if(minim.getTasks().size()==0)
		{
			ok=1;
		}
		minim.addTask(t);

		if(ok==1)
		{
            Thread thread=new Thread(minim);
            thread.start();
		}
	 }
	 public ArrayList<Server> getServer()
	 {
		 return servers;
	 }
	
}
